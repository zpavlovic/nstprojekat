/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nst.project.db;

import com.nst.project.model.AbstractModelObject;
import com.nst.project.model.User;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author User
 */

@Local
public interface DatabaseBroker {
    
    public void create(AbstractModelObject amo);
    
    public void update(AbstractModelObject amo);
    
    public void delete(AbstractModelObject amo);
    
    public AbstractModelObject find(String id, Object object, AbstractModelObject amo);
    
    public List<AbstractModelObject> findAll(String id, AbstractModelObject amo);
    
    public boolean login(User user);
       
}
