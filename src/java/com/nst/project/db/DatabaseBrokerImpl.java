/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nst.project.db;

import com.nst.project.model.AbstractModelObject;
import com.nst.project.model.User;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author User
 */
@Stateless
public class DatabaseBrokerImpl implements DatabaseBroker {

    @PersistenceContext
    EntityManager em;

    @Override
    public void create(AbstractModelObject amo) {
        try {
            em.merge(amo);
        } catch (Exception e) {
            e.getLocalizedMessage();
        }
    }

    @Override
    public void update(AbstractModelObject amo) {
        try {
            AbstractModelObject abstractModel = em.getReference(AbstractModelObject.class, amo.getPK());
            em.merge(abstractModel);
        } catch (Exception e) {
            e.getLocalizedMessage();
        }
    }

    @Override
    public void delete(AbstractModelObject amo) {
        try {
            AbstractModelObject abstractModel = em.getReference(AbstractModelObject.class, amo.getPK());
            em.remove(abstractModel);
        } catch (Exception e) {
            e.getLocalizedMessage();
        }
    }

    @Override
    public AbstractModelObject find(String paramName, Object obj, AbstractModelObject amo) {
          try {
            String query = "SELECT " + amo.getShortTableName() + " FROM " + amo.getTableName() + " WHERE " + amo.getShortTableName() + "." + paramName + " = :" + obj;
            Query q = em.createNativeQuery(query);
            AbstractModelObject abstractModelObject = (AbstractModelObject) q.getSingleResult();
            return abstractModelObject;
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            return null;
        }
    }

    @Override
    public boolean login(User user) {
        try {
            Query loginQuery = em.createNamedQuery("User.login").setParameter("email", user.getEmail()).setParameter("password", user.getPassword());
            User currentUser = (User) loginQuery.getSingleResult();
            return true;
        } catch (Exception e) {
            System.out.println("ERROR: " + e.getLocalizedMessage());
            return false;
        }
    }

    @Override
    public List<AbstractModelObject> findAll(String id, AbstractModelObject amo) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
