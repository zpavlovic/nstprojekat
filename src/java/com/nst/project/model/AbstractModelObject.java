/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nst.project.model;

/**
 *
 * @author User
 */
public interface AbstractModelObject {
    
    public String getTableName();
    
    public String getShortTableName();
    
    public int getPK();
}
